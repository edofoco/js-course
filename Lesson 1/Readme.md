# Javascript Training - Part 1
## Objective
Basic understanding of what a webpage is made of and how to modify and inspect it. By the end of this you should be able to create a static webpage that uses Javascript to manipulate the DOM.

## Lesson Overview
- HTML vs JS vs CSS

- DOM Anatomy

- Scripts vs Stylesheets

- HTML Node Anatomy 

- Basic CSS

- Basic Javascript

- Debug Javascript



